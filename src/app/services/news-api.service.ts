//This service will make http calls to api.
//Input :  N/A
//Output : JSON object

import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class NewsApiService {
  constructor(private _http:Http) {

  }

  getNewsJsonData(url:string){
    return this._http.get(url)
                    .map((res:Response) => res.json());
  }
}
