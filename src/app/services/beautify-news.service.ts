//This service will filter and return only required data.
//Input :  JSON object
//Output : JSON object
// filterNewsPropertiesNEWSAPI : FILTERS DATA FOR NEWS API URL'S RESPONSE
// filterNewsPropertiesTOIAPI : FILTERS DATA FOR TOI API URL'S RESPONSE

import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class BeautifyNewsService {
  constructor() {

  }

  // filters required news properties from NEWSAPI json.
  filterNewsPropertiesNEWSAPI(input:any){

    let _newsItems:News[] = [];

    //input.articles {author,description,publishedAt,title,url,urlToImage}
    input['articles'].forEach(article => {
      let _newsItem:News = new News();

      _newsItem.author = article.author;
      _newsItem.title = article.title;
      _newsItem.description = article.description;
      _newsItem.publishedAt = article.pubDate;
      _newsItem.urlImage = article.urlToImage;
      _newsItem.urlNews = article.url;

      _newsItems.push(_newsItem);
    });
    return _newsItems;
    }

    // filters required news properties from TOIAPI json.
    filterNewsPropertiesTOIAPI(input:any){
      let _newsItems:News[] = [];

      //input.channel.item.{description,guid,link,pubDate,title}
      input['channel']['item'].forEach(article => {
        let _newsItem:News = new News();
        _newsItem.title = article.title;
        _newsItem.description = article.description;
        _newsItem.urlNews = article.link;
        _newsItem.publishedAt = article.pubDate;

        _newsItems.push(_newsItem);
      });
      return _newsItems;
      }
}


// too much coupling
export class News {
    author : string = '';
    description  : string = '';
    publishedAt  : string = '';
    title : string = '';
    urlNews : string = '';
    urlImage  : string = '';
}
