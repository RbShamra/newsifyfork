export class News {

  title : string = '';
  description  : string = '';
  imageURL  : string = '';
  author : string = '';
  dateAdded  : string = '';

  constructor(){
  }
}
