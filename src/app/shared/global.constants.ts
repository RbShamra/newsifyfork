/**
 * Created by rajesh on 01/13/2017.
 */

export class Shared {
  public API_BASEURL: string = 'http://timesofindia.indiatimes.com/rssfeeds/';
  public NEWS_INDIA:string = 'http://timesofindia.indiatimes.com/rssfeeds/-2128936835.cms?feedtype=json';
  public NEWS_SPORTS:string = 'http://timesofindia.indiatimes.com/rssfeeds/4719161.cms?feedtype=json';

  public NEWS_INDIA_NEWSAPI:string = 'https://newsapi.org/v1/articles?source=the-hindu&apiKey=072773db0f4b46f2aa8968a1b5c6c11c';
  constructor(){
  }
}
