import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {NewsApiService,BeautifyNewsService, News} from '../../services/';
import {Shared} from '../../shared';

@Component({
  selector: 'nf-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
  providers:[NewsApiService]
})

export class LandingComponent implements OnInit {

  constructor(private newsApiService:NewsApiService){
    // this.getNews();
  }
  title:string = 'CLICK BUTTONS TO FIND NEWS';
  private shared = new Shared();
  private _beautify = new BeautifyNewsService();
  private newsArray : News[];

  getNews(){
    this.newsApiService.getNewsJsonData(this.shared.NEWS_INDIA_NEWSAPI).subscribe(
      data =>{
        this.newsArray =  this._beautify.filterNewsPropertiesNEWSAPI(data);
        console.log(data);
      })
  }

  getNewsOnClick(newsType:string){
    switch(newsType){
      case 'SPORTS' : this.getSportsNews(); this.title = 'SPORTS NEWS'; break;
      case 'INDIA' : this.getIndiaNews(); this.title = 'INDIA NEWS';  break;
      default : this.getIndiaNews(); break;
      }
    }

    getSportsNews(){
      this.newsApiService.getNewsJsonData(this.shared.NEWS_SPORTS).subscribe(
        data =>{
          this.newsArray =  this._beautify.filterNewsPropertiesTOIAPI(data);
          console.log(data);
        })
    }
    getIndiaNews(){
      this.newsApiService.getNewsJsonData(this.shared.NEWS_INDIA).subscribe(
        data =>{
          this.newsArray =  this._beautify.filterNewsPropertiesTOIAPI(data);
          console.log(data);
        })
    }



















  ngOnInit() {
  }

  //Observable Example
  // dummyData:any;
  // getNewsRefresh(){
  //   let stream$ = new Observable( observer => {
  //     let count: number = 0;
  //     let interval = setInterval(()=>{
  //       observer.next(count++);
  //     },1000);
  //     return () => {
  //       clearInterval(interval);
  //     }
  //   });
  //
  //   stream$.subscribe(value => {
  //     console.log(value)
  //     this.dummyData = value;
  //   });
  // }
}
