import { NewsifyPage } from './app.po';

describe('newsify App', function() {
  let page: NewsifyPage;

  beforeEach(() => {
    page = new NewsifyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
